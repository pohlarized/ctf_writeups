[Challenge 8](https://www.youtube.com/watch?v=JKKIy_a95Fg)
===========
In the eighth challenge we want to find the indice of the first quadruple of
integers, that in sum are equal to an integer k.
The rules for the first quadruple are the same as in challenge 7:  
If `min((index1, index2, index3, index4)) < min((index5, index6, index7, index8))` then the quadruple `(index1, index2, index3, index4)` is considered the first.
