import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/8/"
post_url = "https://cc.the-morpheus.de/solutions/8/"

data = json.loads(requests.get(get_url).text)
k = data["k"]
liste = data["list"]
liste = [
2603, 9440, 3775, 378, 7833, 9300, 6621, 557, 1945, 7090,
8128, 4959, 1029, 6424, 4514, 3725, 8184, 1271, 5626, 6471,
3790, 8723, 1142, 2184, 2111, 210, 1441, 9565, 3965, 7581,
3991, 8499, 294, 1485, 8479, 8773, 113, 9309, 107, 4781,
4523, 7177, 2004, 6428, 4348, 2039, 4483, 3885, 2777, 7424,
8546, 5322, 7581, 5322, 3256, 9410, 82, 6285, 4342, 794,
7104, 4524, 955, 8124, 2811, 3604, 7836, 2313, 1255, 8020,
4158, 3101, 8898, 8042, 9859, 2423, 6514, 9733, 7768, 3116,
208, 3480
]
k = 20580
print(liste[29])
print(liste[52])

# again we just iterate through the list until we find a matching quadruple
for i, x in enumerate(liste):
    # in this challenge tho, the indice can also be the same
    for j, y in enumerate(liste[i:], i):
        for l, z in enumerate(liste[j:], j):
            for m, a in enumerate(liste[l:], l):
                if x + y + z + a == k:
                    solution = [i, j, l, m]
                    break
            else:
                continue
            break
        else:
            continue
        break
    else:
        continue
    break

print(solution)

# payload = {"token": solution}

# print(requests.post(post_url, data=json.dumps(payload)).text)
