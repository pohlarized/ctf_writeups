import requests
import json
import math

get_url = "https://cc.the-morpheus.de/challenges/6/"
post_url = "https://cc.the-morpheus.de/solutions/6/"

num = int(requests.get(get_url).text)
# we know how many digits we need by calculating the binary logarithm of our
# number.
digit_count = int(math.log(num, 2))
digits = []
# we go through the indice of our binary number in reverse order
for x in range(digit_count, -1, -1):
    # we calculate 2 to the power of our current index, if that is smaller than
    # our remaining num then it will become a 1, if its larger than our
    # remaining num it will become a 0
    tmp = num - 2**x
    if tmp >= 0:
        digits.append("1")
        # if we have a 1, we substract 2**x from our remaining num
        num = tmp
    else:
        digits.append("0")

payload = {"token": "".join(digits)}
print(requests.post(post_url, data=json.dumps(payload)).text)
