import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/4/"
post_url = "https://cc.the-morpheus.de/solutions/4/"

data = json.loads(requests.get(get_url).text)
k = data["k"]
liste = data["list"]
# we can calculate the modulo since rotating len(liste) times will give us the
# source list again.
k = k % len(liste)
# to "rotate" we can now just make the last k elements of the list the first k
# elements of the list
new_list = liste[-k:] + liste[:-k]
payload = {"token": new_list}

print(requests.post(post_url, data=json.dumps(payload)).text)
