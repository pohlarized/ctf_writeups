import requests
import json

get_url = "https://cc.the-morpheus.de/challenges/1/"
post_url = "https://cc.the-morpheus.de/solutions/1/"

payload = {"token": requests.get(get_url).text}

# print out the response to get the flag
print(requests.post(post_url, data=json.dumps(payload)).text)

