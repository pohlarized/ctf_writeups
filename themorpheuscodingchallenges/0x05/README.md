[Challenge 5](https://www.youtube.com/watch?v=QXD5otIXBqQ)
===========
In the fifth challenge we have to evaluate a postfix notation maths string.
It contains integers, and the operators `[+, -, *, /]` where `/` is a float
division.  
For the final solution we cut off all decimal places of our resulting float,
but we will calculate the rest as a float.  
### Example:
Heres a step by step evaluation of the string `12 11 10 + - 4 *`:  
`12 11 10 + - 4 *`  
`12 (11 + 10) - 4 *`  
`(12 - (11 + 10)) 4 *`  
`((12 - (11 + 10)) * 4)`

