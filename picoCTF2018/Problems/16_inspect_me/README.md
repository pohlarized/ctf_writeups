In this challenge we get a link to a website and we have to find the flag.  
If we view the source code of the website, we find a comment saying  
```html
<!-- I learned HTML! Here's part 1/3 of the flag: picoCTF{ur_4_real_1nspe -->
```  

Looking further, we also see that some css and some js is getting included,
which each contain a comment aswell:  
```css
/* I learned CSS! Here's part 2/3 of the flag: ct0r_g4dget_e96dd105} */
```
```js
/* I learned JavaScript! Here's part 3/3 of the flag:  */
```

Thus, our flag is `picoCTF{ur_4_real_1nspect0r_g4dget_e96dd105}`
