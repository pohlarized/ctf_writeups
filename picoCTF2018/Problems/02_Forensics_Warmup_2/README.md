In this problem, we have to download a flag.png, but it can not simply be opened as a png.  
Again, my system doesnt trust the cert, so when downloading it via wget i use the `--no-check-certificate` option.

Once we have the file, we check that we *really* cant open the png that easily. If you have a smart image viewer it will already tell you that the file has the wrong extention. However we dont have a smart image viewer, but a quick peak into the file with a text editor will reveal that the headers says `JFIF`, which ususally means that youre looking at a jpeg picture.  
Renaming it to flag.jpg makes our image viewer happy and we can read the flag: `picoCTF{extensions_are_a_lie}