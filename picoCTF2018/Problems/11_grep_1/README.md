In this challenge we are given a file, and we are supposed to find the flag in
it.  
The file ist just a text file, and we know that all flags start with `picoctf`.  
To find our flag, we do `grep "pico" file` and we get the flag as a result: `picoCTF{grep_and_you_will_find_42783683}`  
