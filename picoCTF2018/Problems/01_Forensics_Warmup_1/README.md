First of all we have to download a flag.zip file.  
In my case, this already stated some problems as my system claimed to not trust the ssl certificate for 2018shell.picocft.com.  
However, since i dont really care about its integrity, i just skip the ssl check and download it like this:

```
wget <url> --no-check-certificate
```

Since our instructions are to unzip the file and tell the flag, thats what were gonna do:  

```
$ unzip flag.zip
```

We get a jpeg file. By reading its text we get the flag: `picoCTF{welcome_to_forensics}`