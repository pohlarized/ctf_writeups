In this challenge we get a string `cvpbPGS{guvf_vf_pelcgb!}` and the task asks
us whether we have ever heared of ROT13.  
With our razor sharp minds we deduce that this string is ROT13 encrypted, so we
decrypt it and we get the string `picoCTF{this_is_crypto!}`  
