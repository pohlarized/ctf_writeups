In this challenge we get a host and port to connect to, and we have to look
through the output for the flag.  
Scrolling through it manually is very slow, so we want to save the output to a
file.  
Unlike the title suggests, we dont want to do this with a PIPE, but instead
with the stdout redirect: `nc 2018shell.picoctf.com 34532 > out.txt`.  
Now we get our output in the file `out.txt`.  
To find the flag we do `grep pico out.txt` and we receive `picoCTF{almost_like_mario_b797f2b3}`
