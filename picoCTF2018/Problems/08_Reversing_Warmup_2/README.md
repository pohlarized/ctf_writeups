In this challenge we are given a program, that we are supposed to RUN (monkaS) to retreive the flag.  
Running programs you dont know is always VERY spooky, so dont do this at home!

First, we again download the program via wget:

```
wget --no-check-certificate <url>
```

To execute it, we simply do a

```
./run
```

and we get our flag: `picoCTF{welc0m3_t0_r3VeRs1nG}`
