Our task is to convert a base ten number `27` to a binary number.  
I'm gonna use python for this:
```py
print(bin(27))
```
which returns the string `0b11011`. `0b` is just the prefix denoting that were looking at a binary number.  
This results in our flag being `picoCTF{11011}`.