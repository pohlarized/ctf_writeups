In this challenge, we are given a file `husky.png` and have to find a secret
message in the picture.  
We use the stego tool on the website `https://stylesuxx.github.io/steganography/` and get the flag: `picoCTF{r34d1ng_b37w33n_7h3_by73s}`.  
