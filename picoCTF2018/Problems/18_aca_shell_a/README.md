In this challenge we get a connect, and a little story where some other guy
tells us what to do.  
We just have to follow their orders with some basic linux shell commands, i
have put a summary of the commands i used in order here:

```
cd secrets/
rm intel*
echo 'Drop it in'
cd ..
cd executables
./dontLookHere
whoami
cd ..
cp /tmp/TopSecret passwords
cd passwords
cat TopSecret
```
In the end we get the flag: `picoCTF{CrUsHeD_It_4e355279}`
