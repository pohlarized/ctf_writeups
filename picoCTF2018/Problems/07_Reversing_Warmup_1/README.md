In this challenge we are supposed to convert the string `dGg0dF93NHNfczFtcEwz` from base64 to ASCII.  
On most systems you should have a program calles `base64`, we use that to decode our string:
```
echo -n "dGg0dF93NHNfczFtcEwz" | base64 -d
```
and we get a string: `th4t_w4s_s1mpL3`, so our flag is `picoCTF{th4t_w4s_s1mpL3}`
