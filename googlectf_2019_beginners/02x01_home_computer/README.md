`mount family.ntfs /mnt/gctf`  
in Users/Family/Documents/credentials.txt you get a hint about "extended file
attributes".  
**This solution only works on windows**  
On windows, this might correspond to "alternative data streams", and you can view
them in a windows console via `dir /r`.  
For a particular file, you can also see them with `streams <file>`  

To view the file, we have to first copy it to be a "real" file, and we do that
with `cat <filestream> > something.png`.  
in the png you will find the flag.  
