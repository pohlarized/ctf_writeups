General Skills:
Just lying around somewhere on the ground

Web exploit:
Go through the first tunnel behind the pc, then there is another tunnel at the bottom wall that you cant see.
Go through there and you find the glyph

Binary Exploit:
Alternate going through the blue and red door, starting with the blue one. If
youre doing it right the music will go quicker and quicker each time.
Eventually a golden door appears, go through it and you will get the glyph

Crypto:
Inspect ALL the graveyards. My glyph was somewhere on the right around the middle,
but they might be randomized.

Forensics:
Glyph is in the water on the top right of the room

Reversing:
Just try out all the possible orders you can pull the levers in (its just 24).
At some point the glyph will appear, i believe mine was 3214 but maybe not.

When you get all the glyphs you will get a full Qr code saying this:
`qrcode: password: xmfv53uqkf621gakvh502gxfu1g78glds`

When you go back to the starting room, you can now unlock your computer with
this password. You will see a dialog where some people are discussing passworsd

The password you need is `zerozerozerozero`, so the flag is `picoCTF{zerozerozerozero}`
