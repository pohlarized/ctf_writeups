When we go into the problem folder we just need to execute an `ls -la` to see
all the hidden files, this reveals the file `.cant_see_me`.  
We then cat out the contents of that file:
`cat .cant_see_me` and get the flag: `picoCTF{w3ll_that_d1dnt_w0RK_cb4a5081}`
