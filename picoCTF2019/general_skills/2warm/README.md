"Can you convert the number 42 (base 10) to binary (base 2)?"
```py
print(bin(42))
```
-> `picoCTF{101010}`
