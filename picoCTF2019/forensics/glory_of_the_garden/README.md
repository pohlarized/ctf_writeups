We have a picture `garden.jpg` and look for a flag.
I just opened the picture in VIM and did a search for `picoCTF` and i got the
flag: `picoCTF{more_than_m33ts_the_3y3b7FBD20b}`
