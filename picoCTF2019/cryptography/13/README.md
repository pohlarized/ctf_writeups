We are told that the text `cvpbPGS{abg_gbb_onq_bs_n_ceboyrz}` is encrypted with
ROT13.  
Decrypted, we get: `picoCTF{not_too_bad_of_a_problem}`
