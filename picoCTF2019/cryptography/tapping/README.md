Upon connecting to the netcat session we get the following response:
```
.--. .. -.-. --- -.-. - ..-. { -- ----- .-. ... ...-- -.-. ----- -.. ...-- .---- ... ..-. ..- -. ----. ----- ...-- .---- ....- ----- ....- ....- ---.. }
```
This is morse code, with an online morse code translator we get the following
flag:
`PICOCTF{M0RS3C0D31SFUN903140448}`
