When you read the numbers on the picture you get the following sequence:
`16 9 3 15 3 20 6 { 20 8 5 14 21 13 2 5 18 19 13 1 19 15 14 }`  
This cipher is called A1Z26 because it simply translates the following way:  
A=1  
B=2  
C=3  
...  
Y=25  
Z=26  

We can solve it with a python script:
```py
import string
ALPHABET = " " + string.ascii_uppercase
msg = "16 9 3 15 3 20 6 20 8 5 14 21 13 2 5 18 19 13 1 19 15 14"
letters = [ALPHABET[int(x)] for x in msg.split(" ")]
print "".join(letters)
```
i just removed the `{}` from the message, but we know its gonna start with
picoCTF or as the hint reveals, in this case PICOCTF, this makes our flag:
`PICOCTF{THENUMBERSMASON}`
