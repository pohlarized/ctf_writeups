## Login
- username: `natas0`
- password: `natas0`

## Challenge
We are greeted with the message

>  You can find the password for the next level on this page.

A quick peek into the source code gives away, that the credentials for the next level are in an HTML comment:

> The password for natas1 is g9D9cREhslqBKtcA2uocGHPfMZVzeFK6
