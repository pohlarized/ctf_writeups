## Login
- username: `natas6`
- password: `fOIvE0MDtPTgRhqmmvvAOt2EfXR6uQgR`

## Challenge

We are greeted with a page showing us an input field labeled "Input secret" and a submit button, in addition to a `view sourcecode` link.

Inputing random characters into the input field and clicking submit only yields a message "Wrong secret.".

This is the relevant part of the sourcecode:
```html
<div id="content">

<?

include "includes/secret.inc";

    if(array_key_exists("submit", $_POST)) {
        if($secret == $_POST['secret']) {
        print "Access granted. The password for natas7 is <censored>";
    } else {
        print "Wrong secret";
    }
    }
?>

<form method=post>
Input secret: <input name=secret><br>
<input type=submit name=submit>
</form>
```

We see that at the bottom we have defined out input with the name `secret`.
Now the PHP code first includes a file `includes/secret.inc` and then compared the POST parameter `secret` (which is the text that we submit through our form, as evident by the input name) against a local variable `$secret`, and if they match, gives us the password for the next level.

As we do not see the local variable definition anywhere in this code, we can expect it to be defined in the included file `includes/secret.inc`.

And indeed, browsing to that path we find the following contents:

```php
<?
$secret = "FOEIUWGHFEEUHOFUOIU";
?>
```

So, as we want to get the password, we input this string into our input field and click submit.
And indeed, we get back the message

> Access granted. The password for natas7 is jmxSiH3SP6Sonf8dv66ng8v1cIEdjXWr
