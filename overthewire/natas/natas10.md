## Login
- username: `natas10`
- password: `D44EcsFkLxPIkAAKLosx8z3hxX1Z4MCE`

## Challenge

Again, we are greeted with the same seach interface as in the last challenge, but additionally we now get the message "For security reasons, we now filter on certain characters".

Intruiged by what this might mean, we open the sourcecode to find the following relevant snippet:

```html
<form>
Find words containing: <input name=needle><input type=submit name=submit value=Search><br><br>
</form>


Output:
<pre>
<?
$key = "";

if(array_key_exists("needle", $_REQUEST)) {
    $key = $_REQUEST["needle"];
}

if($key != "") {
    if(preg_match('/[;|&]/',$key)) {
        print "Input contains an illegal character!";
    } else {
        passthru("grep -i $key dictionary.txt");
    }
}
?>
```

Again our input is assigned to the `$key` variable, but this time its value is checked against the regex `[;|&]` thus disallowing us to use `;` and `&` characters (unsure about the `|`, i believe this is interpreted by the regex parser to be a logical OR between the other two characters).

Our payload from the last challenge thus does not work anymore.
However, we can build a new payload without the use of illegal characters.

Remember that we desire the contents of the file `/etc/natas_webpass/natas11`.
We can build a grep command that returns us the contents of the file.
We simply use the empty string as our regex to search for (since every string matches the empty string) and then provide our file as the parameter, so that our payload would look like this: `"" /etc/natas_webpass/natas11`.

When grep gets multiple files passed, it simply scans all of them.
Thus we get the following output:

```
Output:
/etc/natas_webpass/natas11:1KFqoJXi6hRaPluAmk8ESDW4fSysRoIg
dictionary.txt:
dictionary.txt:African
dictionary.txt:Africans
dictionary.txt:Allah
[...]
```

The first line contains our password: `1KFqoJXi6hRaPluAmk8ESDW4fSysRoIg`.
