## Login
- username: `natas2`
- password: `h4ubbcXrWqsTo7GGnnUMLppXbOogfBZ7`

## Challenge

We are greeted with the message

> There is nothing on this page

Looking through the source code, we see that the site is embedding a bunch of javascript and CSS files, but none of them contain the password to the next level.

The comment

> This stuff in the header has nothing to do with the level

Is actually correct, which was unexpected to me.

However, we see that it also embeds an image at the path `files/pixel.png`.
Interesting, could there be other files?
Just browsing to `/files` presents a directory listing with said image, but also a file `users.txt` with the contents:

> # username:password
> alice:BYNdCesZqW
> bob:jw2ueICLvT
> charlie:G5vCxkVV3m
> natas3:G6ctbMJ5Nb4cbFwhpMPSvxGHhQ7I6W8Q
> eve:zo4mJWyNj2
> mallory:9urtcpzBmH

We are of course interested in the password to `natas3`.
