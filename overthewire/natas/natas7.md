## Login
- username: `natas7`
- password: `jmxSiH3SP6Sonf8dv66ng8v1cIEdjXWr`

## Challenge

We are greeted with a webpage with two links `Home` and `About`.
The links respectively link to `/index.php?page=home` and `/index.php?page=about`.
It thus appears as though the `page` GET parameter would determine the webpage we are viewing, and we can thus test out whether there might be other pages around that don't have a link, simply by providing them as the `page` GET parameter.

We thus try to go somewhere else, for example to `/login`.
However, we get the following error back:

```
Warning: include(login): failed to open stream: No such file or directory in /var/www/natas/natas7/index.php on line 21

Warning: include(): Failed opening 'login' for inclusion (include_path='.:/usr/share/php') in /var/www/natas/natas7/index.php on line 21
```

This is interesting!
It indicates that whatever we provide as the `page` parameter is provided as an argument to php's `include` method.

I'm not good at php, but i assume that by default it uses relative paths to the `include_path`.
However, we can perhaps also include absolute paths?

I test this by requesting `/index.php?page=/etc/passwd`, and indeed, we get a password listing:

```
root:x:0:0:root:/root:/bin/bash daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin bin:x:2:2:bin:/bin:/usr/sbin/nologin sys:x:3:3:sys:/dev:/usr/sbin/nologin sync:x:4:65534:sync:/bin:/bin/sync games:x:5:60:games:/usr/games:/usr/sbin/nologin
[...]
```

From the natas frontpage, as well as from a friendly reminder in the HTML source code, we know that all passwords are in `/etc/natas_webpass/natasX` where `X` is replaced with the respective level number.
Thus, we now request `/index.php?page=/etc/natas_webpass/natas8` and get the following response back:

```
a6bZCNYwdKqN5cGP11ZdtPg0iImQQhAB
```
