# [Challenge 0x0c](https://www.youtube.com/watch?v=SFal_EaxA_E)
**Unfortunately the challenge interface is down as i am writing this, but i can
still access the relevant "hacking" part of the challenge**  
When we open the website, we see a very simple html page.  
The source is tiny, and does not really help us. In retrospect, you might say
that the comment is helpful, but in the end it does not really help us.  
First of all, we want to find out whether there are other places on this server
to look at. We use gobuster for this:


```
gobuster -u http://the-morpheus.de:20013 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
```

This should quickly find the `/upload` subdirectory.  
Here it is a little bit of guessing, but there has been a tiny hint on the
frontpage. The title of the previous video was about json and xml.  
If we upload a json file, we will notice that nothing happens.  
For most other file formats that i have checked, the site simply ignores your
uploads.  
However, if you upload an xml file, the site starts to process it. Great!  
The upload page specifies that you want a title, url and description attribute.  
So our initial xml file looks like this:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<content>
    <title> My Title </title>
    <url>https://google.de</url>
    <description> 12345 </description>
</content>
```

Now we need to think about what you could possibly do with arbitrary xml file
uploads? Right, XXE!  
*IIRC the description of the challenge was that you want to find the IP, so the
payload will do that. If you understand XXE, it will be pretty easy for you to
change the payload.*  

Unfortunately we dont get RCE out of this XXE, however we can still read
arbitrary files.  
What we want is the local network IP, which you can usually find in
`/proc/net/fib_trie`.
I have used a sample XXE xml file from owasp, and just exchanged the payload
and added our needed attributes:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
  <!DOCTYPE foo [
   <!ELEMENT foo ANY >
   <!ENTITY xxe SYSTEM "file:///proc/net/fib_trie" >]>
   <foo>
        <title>&xxe;</title>
        <url/>
        <description/>
   </foo>
```

After uploading this file, we will see the output of the `fib_trie` file in our
title attribute.  
That was all the hacking, we now find our local net ip next to the `host LOCAL`
description.
