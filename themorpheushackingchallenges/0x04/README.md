# [Challenge 0x04](https://www.youtube.com/watch?v=Ck1bt1JHmz0)
In this challenge the sourcecode of the website looks pretty boring again.  
We are getting introduced to cookies here. The cookie gets stored locally and we can look at it and modify it.  
Open your browsers developer console and look at `document.cookie`, you will se the value `isAdmin=0`.  
Simply change it to 1 via `document.cookie="isAdmin=1"` and reload your website and you get your flag.
