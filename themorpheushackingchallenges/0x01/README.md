# [Challenge 0x01](https://www.youtube.com/watch?v=lxyDT_RSSsI)
This introduction challenge is pretty easy, looking at the sourcecode we find the `checkPassword()` function which simply checks for two different possible passwords.  
The first possible password is `superGeheimnis`, the second one only compares to an md5 hash with the value `e206a54e97690cce50cc872dd70ee896`.  
If you just google for this hash value you will find out that the password is `linux`.
For some reason, at this point only the second password works for the flag.