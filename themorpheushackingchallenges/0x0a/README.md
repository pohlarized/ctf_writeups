# [Challenge 0x0a](https://www.youtube.com/watch?v=Wr5Fy1OnGXk)
As this challenge was an easter special, it was not very difficult.  
We have a page with a link again, and a login form as usual.  
Viewing the source shows that the link points to `redirect.php`, and when we click the link we get redirected to the youtube video of this challenge.  
There are different ways to solve this challenge, you could probably bruteforce the password in somewhat reasonable time, you can intercept the redirection with something like burpsuit, or you can download the `redirect.php` via your browser.  
In the two latter cases you will end up with the sourcecode of the `redirect.php` which gives you the password: `pbm3GtHD`.  
If we just login as `admin` with that password, we get redirected to another page with a picture in it.  
Viewing the sourcecode again will reveal the flag.  
