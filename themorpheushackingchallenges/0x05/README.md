# [Challenge 0x05](https://www.youtube.com/watch?v=qF-TKx_Vhtw)
For this challenge, we already know that we get a cookie with the value `token=cb421db012e76884d9; isAdmin=0` and that we have to reverse the token somehow.  
In the video the underlying php is revealed, and shows us how the token is created.  
It uses an OTP encryption to encrypt either `isAdmin=0` or `isAdmin=1` depending on the value, so we probably currently have the encrypted version of `isAdmin=0` and we want to find out what value the token has if we get the value `isAdmin=1`.   
The OTP encryption works by first using an ASCII to HEX converter on the string `isAdmin=0/1`, then XORs it with an unknown secret key.   
Luckily XOR is symmetric, so if we know the encrypted text and the plaintext, we can simply XOR those and get our secret key.    

*At this point, if you think about it enough you could probably already guess the new token, but we will continue with a strict method first and explain it afterwards.*  

First of all, we create the HEX encoded string for `isAdmin=0`: `697341646d696e3d30`.  
Now we get our secret key: `cb421db012e76884d9 ^ 697341646d696e3d30 = a2315cd47f8e06b9e9`.  
Now we want to reencrypt our `isAdmin=1` with that secret key. So first we get the HEX encoded string for `isAdmin=1`: `697341646d696e3d30`.  
Lets encrypt that with our secret key: `697341646d696e3d30 ^ 697341646d696e3d30 = cb421db012e76884d8`.  
Now we are all set to change our cookie:  

```js
document.cookie="isAdmin=1";
document.cookie="token=cb421db012e76884d8";
```

Reloading the page will reveal your cookie.  
#### Bonus:
As i mentioned you could already guess, or very easily get the new token already without knowing the secret key.  

```
secret_key = token XOR isAdmin=0
new_token = secret_key XOR isAdmin=1
=>
new_token = token XOR isAdmin=0 XOR isAdmin=1
```

Since ASCII converts a string character by character, and HEX is just a different representation of the ASCII values, we know that the strings `isAdmin=0` and `isAdmin=1` will be nearly identical, only the last character of that string is different.  
By looking up the ASCII values of 0 and 1, or just by having an educated guess you could quickly figure out that they have the ASCII values `48` and `49` respectively, which represented in binary would only make for a one bit difference.  
Thus, for your new token you are essentially doing `token XOR 1`, and we can confirm this by looking at our two tokens, they only differ in the last character switching from `9` to `8`.
