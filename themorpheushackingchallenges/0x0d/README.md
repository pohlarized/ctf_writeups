# [Challenge 0x0d](https://www.youtube.com/watch?v=7Wyccg_z9eA)
**Unfortunately the challenge interface is down as i am writing this, but i can
still access the relevant "hacking" part of the challenge**  

First of all, while this challenge is technically accessible through your webbrowser, it may delete alot of the formatting
necessary to understand the challenge. If you want to make it pretty difficult for yourself, you can still try to do it through
your webbrowser exclusively. 

However, i am going to use `wget` to look at the websites.

```
wget http://the-morpheus.de:20014
```

will download the `index.html` with the following content:


```
NNNNNNNN        NNNNNNNN   SSSSSSSSSSSSSSS              AAA
N:::::::N       N::::::N SS:::::::::::::::S            N:::A
S::::::::N      E::::::NC:::::SUSRSI::::::S           T:::::Y
N:::::::::N     N::::::NS:::::S     SSSSSSS          A:::::::A
N::::::::::N    N::::::NS:::::S                     A:::::::::A
N:::::::::::N   N::::::NS:::::S                    A:::::A:::::A
T:::::::H::::N  R::::::O U::::SSSS                G:::::A H:::::A
N::::::N N::::N N::::::N  SS::::::SSSSS          A:::::A   A:::::A
N::::::N  N::::N:::::::N    SSS::::::::SS       A:::::A     A:::::A
N::::::N   N:::::::::::N       SSOBSC::::S     A:::::AURAIAATY:::::A
0::::::N    N::::::::::N            x:::::S   A:::::::::::::::::::::A
N::::::N     N:::::::::N            S:::::S  A:::::AA0AAAAAAAAAA:::::A
N::::::N      N::::::::NSSSSSSS     S:::::S A:::::A             A:::::A
N::::::N       N:::::::NS::::::SdSSSS:::::SA:::::A               A:::::A
N::::::N        N::::::NS:::::::::::::::SSA:::::A                 A:::::A
NNNNNNNN         NNNNNNN SSSSSSSSSSSSSSS AAAA.sA                   AAAAAAA
```

Epic art. You notice that there is a big N made out of Ns, an S made out of Ss and and A made out of As.  
But if you look closer, there are some letters sometimes that dont quite fit in. Now this part of the challenge is solveable by hand,
however a quick python script will give us a pretty good estimate of the hidden message:

```py
inp = """
NNNNNNNN        NNNNNNNN   SSSSSSSSSSSSSSS              AAA
N:::::::N       N::::::N SS:::::::::::::::S            N:::A
S::::::::N      E::::::NC:::::SUSRSI::::::S           T:::::Y
N:::::::::N     N::::::NS:::::S     SSSSSSS          A:::::::A
N::::::::::N    N::::::NS:::::S                     A:::::::::A
N:::::::::::N   N::::::NS:::::S                    A:::::A:::::A
T:::::::H::::N  R::::::O U::::SSSS                G:::::A H:::::A
N::::::N N::::N N::::::N  SS::::::SSSSS          A:::::A   A:::::A
N::::::N  N::::N:::::::N    SSS::::::::SS       A:::::A     A:::::A
N::::::N   N:::::::::::N       SSOBSC::::S     A:::::AURAIAATY:::::A
0::::::N    N::::::::::N            x:::::S   A:::::::::::::::::::::A
N::::::N     N:::::::::N            S:::::S  A:::::AA0AAAAAAAAAA:::::A
N::::::N      N::::::::NSSSSSSS     S:::::S A:::::A             A:::::A
N::::::N       N:::::::NS::::::SdSSSS:::::SA:::::A               A:::::A
N::::::N        N::::::NS:::::::::::::::SSA:::::A                 A:::::A
NNNNNNNN         NNNNNNN SSSSSSSSSSSSSSS AAAA.sA                   AAAAAAA
"""

print(inp.replace('\n', '')
      .replace('N', '')
      .replace('S', '')
      .replace('A', '')
      .replace(':', '')
      .replace(' ', ''))
```
This will give us the output `ECURITYTHROUGHOBCURITY0x0d.s`.  
With a bit of imagination you might find out that the message wants to say `security through obscurity 0x0d.s`.  
You might know that `.s` files are typically assembly source files, but where do we get this file from?  
A bit of experimenting will show that its is a file in the web root, so doing a
```
wget the-morpheus.de:20014/0x0d.s
```
will give us this assembly file:

```s
.file	"0x0c.c"
	.text
	.section	.rodata
.LC0:
	.string	"Pa"x
.LC1:
	.string	"h15"
.LC2:
	.string	"wb0" 
.LC3:
	.string	"u77"
.LC4:
	.string	"rd: "x
.LC5:
	.string	"wo"x
.LC6:
	.string	"ss"x
.LC7:
	.string	"h0"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movq	%rax, -1104(%rbp)
	leaq	.LC1(%rip), %rax
	movq	%rax, -1096(%rbp)
	leaq	.LC2(%rip), %rax
	movq	%rax, -1088(%rbp)
	leaq	.LC3(%rip), %rax
	movq	%rax, -1080(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, -1072(%rbp)
	leaq	.LC5(%rip), %rax
	movq	%rax, -1064(%rbp)
	movq	-1104(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	leaq	.LC6(%rip), %rax
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	movq	-1064(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	movq	-1072(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	leaq	.LC7(%rip), %rax
	movq	%rax, -1048(%rbp)
	movq	-1048(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
	movq	-1088(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	movq	-1080(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	movq	-1096(%rbp), %rdx
	leaq	-1040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat@PLT
	movl	$0, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L3
	call	__stack_chk_fail@PLT
.L3:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 7.3.1 20180312"
	.section	.note.GNU-stack,"",@progbits
```

Now, if you know assembly it might be easiest for you to just add in a print to print out the password string.  
However, if youre not very good at assembler, simply guessing the password is probably the easiest, since it is
conveniently written in 1337-speak, and the string parts are defined at the very top.

Finding out that the first part of the string is `Password: ` is rather easy,
and it leaves us with the string parts `h15`, `wb0`, `u77` and `h0`.  
From here, we have 4!=24 possible combinations to try out, which will eventually lead us to `Password: h0wb0u77h15`.  