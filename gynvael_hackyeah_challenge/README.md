The challenge is currently live on `https://gynvael.coldwind.pl/hackyeah2019.php`
but i dont know how long it will stay up.  

Just in case, here is what we can see on the website:
```php
<?php
// The flag is also the 100% discount code for the HackYeah 2019 hackathon.
// The discount code will work for the first 30 uses, and I have no way to
// count how many uses still remain so... just try it.
// The hackathon takes place 14-15.09.19, so after that date this challenge
// becomes just a fun exercise with no prizes.
// Good luck!
// -- gynvael
//
// P.S. Here's a hint: PHP's crc32 might be a different crc32 than in other
// languages. There are actually a lot of different crc32 functions.
//

if (!isset($_GET['hack'])) {
  highlight_file("hackyeah2019.php");
  die();
}

$hack = $_GET['hack'];
if (!is_array($hack)) {
  die("oh no!");
}

$used_up_keys = array();
$next_key = "start";

for ($i = 0; $i < 32; $i++) {
  if (!array_key_exists($next_key, $hack) ||
      !is_string($hack[$next_key])) {
    die("oh my!");
  }

  $value = $hack[$next_key];
  $hash = substr(hash("crc32", $value), 0, 5);
  if ($hash !== "31337") {
    die("oh bummer!");
  }

  if (array_key_exists($value, $used_up_keys)) {
    die("oh well!");
  }

  $used_up_keys[$value] = true;
  $next_key = $value;
}

$GLOBALS['gimme_flag'] = true;
include_once('hackyeah2019_secret.php');
```

This is the code of the very website we are visiting. You can see in the first
if block, if the GET parameter `hack` is not set, the site shows us the file
`hackyeah2019.php`.  
So our end goal is to make it through the code, all the way to the bottom to
get the global variable `gimme_flag` set to true, and have the secret
`hackyeah2019_secret.php` file included.  

Lets analyze the code block by block:
```php
$hack = $_GET['hack'];
if (!is_array($hack)) {
  die("oh no!");
}
```
The variable `$hack` gets set from the GET parameter `hack`.  
It is then tested whether the variable is an array, and if not the script ends
with the "error message" `oh no`.  
This meant, we will have to provide a GET parameter `hack` that the script
interprets as an array. This can be done like this: `?hack[]`.  
There are more fancy ways to set different array fields, but this is enough to
create a basic array.  
Next we see, that an empty array `$used_up_keys` is created, along with a
variable `$next_key` with the initial value "start".  

We now get into a for loop, running from `$i = 0` to `$i < 32` incrementing
`$i` by one in each iteration.  
But lets also look at the different blocks inside the for loop individually:  
```php
  if (!array_key_exists($next_key, $hack) ||
      !is_string($hack[$next_key])) {
    die("oh my!");
  }
```
This block checks, whether the current value of `$next_key` is a key in the
`$hack` array, and whether the value corresponding to that key is a string.  
This means, we will need to provide at least an array with a key "start" and a
value that is some string. Luckily, as far as i know GET parameters provided
through the URL are automatically casted to strings, which is helpful for us
because then this will already help us getting through this block in the first
iteration: `hack[start]=1234`.  

```php
  $value = $hack[$next_key];
  $hash = substr(hash("crc32", $value), 0, 5);
  if ($hash !== "31337") {
    die("oh bummer!");
  }
```
In this block, first of all the value of `$hack[$next_key]` is stored in
`$value`.  
Then, we store the first 5 characters of *the* crc32 hash of `$value` in
`$hash`.  
Here you have to pay attention, as the comment says there is not *the one*
crc32 algorithm, and in particular this one is the BZIP2-CRC32 aka ITU I.363.5
algorithm.  
I found that this algo is not very common, and thus i ended up coding the
solution mostly in php, since obviously it has this algorithm built in.  
Either way, now we know that the value of each `$next_key` key in our `$hack`
array has to have a crc32 that starts with `31337`.  
In the last block
```php
  if (array_key_exists($value, $used_up_keys)) {
    die("oh well!");
  }

  $used_up_keys[$value] = true;
  $next_key = $value;
```
It is tested whether `$value` exists as a key in `$used_up_keys`, and if so we
error our with the message `oh well`.  
If not, `$value` is added to `$used_up_keys`, and `$next_key` is set to
`$value`.  
So, the value of the current iteration will be the key of the next iteration,
and we need to have unique values.  
In summary, this is what we need:
A 32 element array with 32 unique values. All values must be strings that have
a crc32 starting with "31337". The array also must be traversable in a "chain",
the initial key is "start", and from there on each value mus also correspond to
a key (except for the last one).  
For example, this array would be traversable in a chain:
```
"start" => "1234",
"1234" => "asdf",
"asdf" => "qwer",
"qwer" => "0000"
```

Now all we have to do is to write a script, that creates us an array like this,
preferably in a form that we can just paste the output into our browser and we
will get back the flag.  
I know a nices solution would be to have the script build the array, and then
send a request to the URL itself, however i dont know PHP and this works.  
The idea of the script is simple, generate random numbers, convert them into
strings and calculate their crc32s. If the crc32 starts with "31337", we
remember the corresponding string to make sure we generate unique strings, and
then generate the next one. We do this until we get 32 values.  
In my case, i dont only remember the strings but i also print them out right
away, in a format that i can paste at the end of my browser URL to get the flag
returned.  
My generation script looks like this:  
```php
<?php

$used_up_keys = array();
$last = "start";

for ($i = 0; $i < 32;){

  $val = rand();
  $str = strval($val);
  $full_hash = hash("crc32", $str);
  $short_hash = substr($full_hash, 0, 5);
  if ($short_hash === "31337"){
    if(!array_key_exists($str, $used_up_keys)){
      print "hack[";
      print $last;
      print "]=";
      print $str;
      print "&";
      $i += 1;
      $used_up_keys[$last] = $str;
      $last = $str;
    }
  }

}
?>
```
It produces an output that looks like this:
```
hack[start]=127029114&hack[127029114]=829129514&hack[829129514]=1493471519&hack[1493471519]=776269812&hack[776269812]=1117631902&hack[1117631902]=636418404&hack[636418404]=415517013&hack[415517013]=469260262&hack[469260262]=1547999279&hack[1547999279]=808315625&hack[808315625]=1618898662&hack[1618898662]=580612689&hack[580612689]=1942216078&hack[1942216078]=1726533871&hack[1726533871]=2029266687&hack[2029266687]=1689544202&hack[1689544202]=886362000&hack[886362000]=2071551195&hack[2071551195]=1584681276&hack[1584681276]=1744212960&hack[1744212960]=1712599368&hack[1712599368]=178738074&hack[178738074]=168297314&hack[168297314]=903295654&hack[903295654]=105345374&hack[105345374]=1212500985&hack[1212500985]=613752016&hack[613752016]=1323879117&hack[1323879117]=729141036&hack[729141036]=1373355816&hack[1373355816]=1907162841&hack[1907162841]=874462454&
```
To use it, just remove the last & and prepend a ?, the full url would look like
this:
```
https://gynvael.coldwind.pl/hackyeah2019.php?hack[start]=127029114&hack[127029114]=829129514&hack[829129514]=1493471519&hack[1493471519]=776269812&hack[776269812]=1117631902&hack[1117631902]=636418404&hack[636418404]=415517013&hack[415517013]=469260262&hack[469260262]=1547999279&hack[1547999279]=808315625&hack[808315625]=1618898662&hack[1618898662]=580612689&hack[580612689]=1942216078&hack[1942216078]=1726533871&hack[1726533871]=2029266687&hack[2029266687]=1689544202&hack[1689544202]=886362000&hack[886362000]=2071551195&hack[2071551195]=1584681276&hack[1584681276]=1744212960&hack[1744212960]=1712599368&hack[1712599368]=178738074&hack[178738074]=168297314&hack[168297314]=903295654&hack[903295654]=105345374&hack[105345374]=1212500985&hack[1212500985]=613752016&hack[613752016]=1323879117&hack[1323879117]=729141036&hack[729141036]=1373355816&hack[1373355816]=1907162841&hack[1907162841]=874462454
```

This gives us the flag:
`Gynvael_Coldwind_and_HackYeah`

---

Since i was interested in the bruteforcing part i played around with some
methods. Essentially, our crc32 is a 32bit integer, so we have 2^32 different
possibilities of getting a crc32 (or since its in hex representation, 16^8).  
Now, since our first five hex digits are fixed, we still have 3 hex digits of
range where valid checksums could be. Thats 16^3 = 4096. So, out of 16^8
possible checksums, we want one of 16^3 "valid" checksums.  
To find the first match, we need an average of 16^8 / 16^3 = 16^5 = 1048576
tries, given that the crc32 algorith is more or less uniformly distributed.  
Now, since we need unique hashes the chance to find the next one decreases by a
tiny amount for every iteration, but it is basically negligible. The lower
bound is 16^8 / (16^3 - 31) = 1056572,5.  
For good measure, we just take the middle of these two chances and multiply
them by 32 because we want to find 32 hashes. This gives us an estimated
~33682378 tries to find 32 values whose crc32 hashes start with `31337`.  

Now, i came up with 3 methods to test this:  

### Random
In every iteration, i pick a random number using the buit in `random` function.  
I then hash this number with crc32 and see if it fits our criteria.  
I have averaged 20 rounds of finding 32 unique values with such crc32 hashes
each, and ended up with an average of ~33096664 tries. Pretty close to our
estimat!

### Counting up
This is probably the simplest way. Just count up from 0, hash that number with
crc32 and see what happens. This is also deterministic, so i can give you a
list of the first 32 positive numbers whose crc32 hashes start with `31337`:
(note: duplicates *could* happen, since were looking for unique crc32s they
wont be in the list)
```
498696	31337a92
680822	31337cf5
877876	31337d54
2089887	313370b5
2291633	313376d2
4584876	31337c8c
5879951	31337438
6929802	31337809
8227856	31337794
9335791	313378e6
9514061	3133734a
10838853	313375fb
12495827	3133717e
12906299	31337371
13968904	313379ca
14380662	31337b20
18423642	31337038
19531985	31337f4a
19729731	3133792d
20256927	31337f56
20898037	3133733c
21565114	31337b88
22435043	313377b9
23306878	31337367
24719742	3133702c
25413631	31337939
26362092	31337ea4
26543762	31337508
27649611	31337c1d
31960190	313379b1
33290254	31337351
38515402	313376c9
```

### Rehashing random sha256
Now this method is not much different from the random method, it is just in
case our random number generator that the php built in `rand()` function uses
creats bad random numbers, and by chance its number generator somehow
correlates with the way crc32 works and thus skews its output in a specific
direction. By using a cryptographically secure hash algo, we make sure that
there is no pattern in the strings we feed to crc32, eliminating any possible
weird correlations coming from bad randomization. I didnt think that the built
in rng was bad anyways, but this is just making sure.  
One thing is certain, this increases the calculation time by ~factor 5.  
So, i have again averaged over 20 rounds of finding 32 unique crc32s starting
with `31337` each, and ended up with an average of `33028912` tries.  
Again pretty close to our estimate.  

So by this experiment, we have proven that maths work and statistics is a
thing. Very cool.
